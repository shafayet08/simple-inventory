from django.shortcuts import render, redirect
from django.http import HttpResponse
from . models import Product, Order, Customer
from . forms import *
from django.forms import inlineformset_factory
from . filters import OrderFilter
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from . decorators import unauthenticated_user, allowed_users, admin_only
from django.contrib.auth.models import Group


# Create your views here.
@login_required(login_url='accounts:loginPage')
@admin_only
def dashboard(request):
    customers = Customer.objects.all()
    last_five_customer = Customer.objects.all().order_by('-id')[:5]

    orders = Order.objects.all()
    last_five_orders = Order.objects.all().order_by('-id')[:5]

    total_customers = customers.count()
    total_orders = orders.count()
    total_delivered = orders.filter(status='Delivered').count()
    total_pending = orders.filter(status='Pending').count()

    context = {
        'last_five_orders': last_five_orders,
        'last_five_customer': last_five_customer,
        'customers': customers,
        'orders': orders,
        'total_customers': total_customers,
        'total_orders': total_orders,
        'total_delivered': total_delivered,
        'total_pending': total_pending,

    }
    return render(request, 'accounts/dashboard.html', context)

@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['admin'])
def all_orders(request):
    customers = Customer.objects.all()

    orders = Order.objects.all()

    total_customers = customers.count()
    total_orders = orders.count()
    total_delivered = orders.filter(status='Delivered').count()
    total_pending = orders.filter(status='Pending').count()

    context = {
        'customers': customers,
        'orders': orders,
        'total_customers': total_customers,
        'total_orders': total_orders,
        'total_delivered': total_delivered,
        'total_pending': total_pending,

    }
    return render(request, 'accounts/all_orders.html', context)


@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['admin'])
def products(request):
    products = Product.objects.all()
    context = {
        'products': products
    }
    return render(request, 'accounts/products.html', context)


@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['admin'])
def customer(request, pk):
    customer = Customer.objects.get(pk=pk)
    orders = customer.order_set.all()
    order_count = orders.count()

    myFilter = OrderFilter(request.GET, queryset=orders)
    orders = myFilter.qs
    context = {
        'customer': customer,
        'orders': orders,
        'order_count': order_count,
        'myFilter': myFilter,
    }
    return render(request, 'accounts/customer.html', context)


@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['admin'])
def create_order(request, pk):
    OrderFormSet = inlineformset_factory(Customer, Order, fields=('product', 'status'), extra=10)
    customer = Customer.objects.get(pk=pk)
    formset = OrderFormSet(queryset=Order.objects.none(), instance=customer)
    # form = CreateOrderForm(initial={'customer': customer})
    if request.method == 'POST':
        formset = OrderFormSet(request.POST, instance=customer)
        if formset.is_valid():
            formset.save()
            return redirect('accounts:dashboard')

    context = {
        'formset': formset
    }

    return render(request, 'accounts/create_order.html', context)


@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['admin'])
def update_order(request, pk):
    order = Order.objects.get(pk=pk)
    form = CreateOrderForm(instance=order)
    if request.method == 'POST':
        form = CreateOrderForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            messages.success(request, f'{order} has been updated.')
            return redirect('accounts:dashboard')

    context = {
        'form': form
    }

    return render(request, 'accounts/update_order.html', context)


@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['admin'])
def delete_order(request, pk):
    order = Order.objects.get(pk=pk)
    if request.method == 'POST':
        order.delete()
        messages.success(request, f'{order} has been deleted.')
        return redirect('accounts:dashboard')

    context = {
        'order': order
    }

    return render(request, 'accounts/delete_order.html', context)


@unauthenticated_user
def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Accounts has been successfully created for {username} ')
            return redirect('accounts:loginPage')

    context = {
        'form': form,
    }
    return render(request, 'accounts/register.html', context)

@unauthenticated_user
def loginPage(request):
    if request.user.is_authenticated:
        return redirect('accounts:dashboard')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password =request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('accounts:dashboard')
            else:
                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, 'accounts/login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('accounts:loginPage')


@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['customer'])
def userPage(request):
    orders = request.user.customer.order_set.all()

    total_orders = orders.count()
    total_delivered = orders.filter(status='Delivered').count()
    total_pending = orders.filter(status='Pending').count()

    context = {
        'orders': orders,
        'total_orders': total_orders,
        'total_delivered': total_delivered,
        'total_pending': total_pending,
        'orders': orders,

    }
    return render(request, 'accounts/user.html', context)

# Customer Profile
@login_required(login_url='accounts:loginPage')
@allowed_users(allowed_roles=['customer'])
def accountSettings(request):
    customer = request.user.customer
    form = CustomerForm(instance=customer)

    if request.method == 'POST':
        form = CustomerForm(request.POST, request.FILES, instance=customer)
        if form.is_valid():
            form.save()
            messages.success(request, f'Accounts has been updated')

    context = {
        'form': form
    }

    return render(request, 'accounts/account_settings.html', context)




from accounts.forms import ModelPurchaseForm
from django.forms.formsets import formset_factory
def create_purchase(request):
    formset = formset_factory(ModelPurchaseForm, extra=3)

    if request.method == 'POST':
        formset = formset(request.POST)
        if formset.is_valid():
            print('form is valid')
        else:
            if formset.errors:
                print(formset.errors)
    context = {
        'formset': formset,
    }

    return render(request, 'accounts/formset.html', context)
