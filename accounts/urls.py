from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

app_name='accounts'
urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('products/', views.products, name='products'),
    path('all_orders/', views.all_orders, name='all_orders'),
    path('customer/<int:pk>/', views.customer, name='customer'),

    path('create_order/<int:pk>/', views.create_order, name='create_order'),
    path('update_order/<int:pk>/', views.update_order, name='update_order'),
    path('delete_order/<int:pk>/', views.delete_order, name='delete_order'),

    path('logoutUser/', views.logoutUser, name='logoutUser'),
    path('loginPage/', views.loginPage, name='loginPage'),
    path('register/', views.register, name='register'),
    path('user/', views.userPage, name='userPage'),

    path('create-purchase/', views.create_purchase, name='create_purchase'),


    path('user/', views.userPage, name='userPage'),

    # Password Reset

]

'''
1 - Submit email form                         //PasswordResetView.as_view()
2 - Email sent success message                //PasswordResetDoneView.as_view()
3 - Link to password Rest form in email       //PasswordResetConfirmView.as_view()
4 - Password successfully changed message     //PasswordResetCompleteView.as_view()
'''
